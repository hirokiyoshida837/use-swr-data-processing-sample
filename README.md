# swr-data-processing-sample

## about

this is sample project for data processing with [vercel/swr](https://github.com/vercel/swr)


refs : [Next.js / React でswrを使う際、データの加工をいい感じにする方法 ($2091207) · Snippets · Snippets · GitLab](https://gitlab.com/-/snippets/2091207)


## Licence

MIT
