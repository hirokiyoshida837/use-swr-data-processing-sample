import styles from '../styles/Home.module.css'
import {NextPage} from "next";
import React from "react";
import {useSearchUserRepository} from "../src/custom-hooks";


const Page: NextPage = () => {

  const {data} = useSearchUserRepository("twitter", "Scala");

  return (
    <div className={styles.container}>

      <h1> hello world!</h1>
      <h2>
        {JSON.stringify(data)}
      </h2>

    </div>
  )
}

export default Page;
