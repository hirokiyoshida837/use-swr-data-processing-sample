import useSWR, {SWRResponse} from "swr";
import {GitHubUser, Repository} from "./domain/types";

/**
 * get the user repository list which using specific language from GitHub REST API
 * @param user GitHub UserID
 * @param language search target language. ex: Scala, Java, etc...
 */
export const useSearchUserRepository = (user: string, language: string): SWRResponse<Array<string>, any> => {

  // 1. get the user data from GitHub REST API
  const userBaseURL = `https://api.github.com/users/${user}`
  const userData = useSWR<GitHubUser>(userBaseURL);

  // 2. get the repository list from 1. API call result.
  // for set deps of second useSWR request, you should set before SWRResponse.data to args or key.
  const repoData = useSWR<Array<Repository>>(userData.data ? userData.data.repos_url : null);

  // 3. filter the repository list by language, and map to repository name list.
  const list = useSWR<Array<string>>(repoData.data ? ["searchUserRepository", user, language] : null, () => {

    // if you want to processing or merge some api-call result,
    // write the code here.
    return repoData.data ? repoData.data
      .filter(x => x.language === language)
      .map(x => x.name)

      // you put this fixed phrase, otherwise 3rd useSWR() response type include undefined like below
      : new Promise<Array<string>>(_=> _);
  })

  //  const list = useSWR<Array<string> | undefined>(repoData.data ? ["searchUserRepository", user, language] : null, () => {
  //
  //     // if you want to processing or merge some api-call result,
  //     // write the code here.
  //     return repoData.data && repoData.data
  //       .filter(x => x.language === language)
  //       .map(x => x.name);
  //   })


  console.log(list);


  return list
}

